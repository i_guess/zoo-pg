package edu.baylor.zoo.service;

import java.io.Serializable;

public class ServiceResponse implements Serializable {
    
    private Boolean result;
    private String description;

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
