package edu.baylor.zoo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import edu.baylor.zoo.model.Animal;
import edu.baylor.zoo.model.Habitat;
import edu.baylor.zoo.model.NaturalEnvironment;
import edu.baylor.zoo.model.Species;
import edu.baylor.zoo.repositories.AnimalRepository;
import edu.baylor.zoo.repositories.HabitatRepository;

@Stateless
public class AnimalRegistration {

    private static String ANIMAL_CHECKIN_RULE_VIOLATION_MESSAGE = "Animal can be placed only to a habitat of proper type that is not full. Animal can not be placed to animals of other species.";

    @Inject
    private Logger log;

    @Inject
    private Event<Animal> animalEventSrc;

    @Inject
    private AnimalRepository animalRepo;

    @Inject
    private HabitatRepository habitatRepo;
    
    public void easySave(Animal animal) {
    	log.info("Registering unchecked animal");
    	animalRepo.save(animal);
    }

    public ServiceResponse save(Animal animal, Habitat habitat) {
        log.info("Registering " + animal.getName());
        ServiceResponse response = new ServiceResponse();
        if ((animal.getDescription().getNaturalEnvironment() != habitat.getType())
                || (!habitat.getAnimals().isEmpty() && habitat.getAnimals().get(0).getDescription() != animal.getDescription())
                || (habitat.getCapacity() == habitat.getAnimals().size())) {
            response.setResult(false);
            response.setDescription(ANIMAL_CHECKIN_RULE_VIOLATION_MESSAGE);
        } else {
            try {
                animal.setHabitat(habitat);
                habitat.getAnimals().add(animal);
                animalRepo.save(animal);
            } catch (Exception ex) {
                response.setResult(false);
                response.setDescription("Error");
                log.severe(ex.toString());
                return response;
            }
            response.setResult(true);
            response.setDescription(String.format("%s, %d thrown to the cage %s, %d", animal.getName(),
                    animal.getId(), habitat.getName(), habitat.getId()));
            animalEventSrc.fire(animal);

        }
        log.info(String.format("result = %b, desc = %s", response.getResult(), response.getDescription()));
        return response;
    }

    public List<Habitat> findFriends(Species animalType) {
        return habitatRepo.findVacantFriends(animalType);
    }

    public List<Habitat> findEmptyHabitats(NaturalEnvironment env) {
        return habitatRepo.findEmptyByEnvironment(env);
    }

    public List<Habitat> getHabitatSuggestionList(Species species) {
        ArrayList<Habitat> result = new ArrayList<Habitat>();
        result.addAll(findFriends(species));
        result.addAll(findEmptyHabitats(species.getNaturalEnvironment()));
        return result;
    }
}
