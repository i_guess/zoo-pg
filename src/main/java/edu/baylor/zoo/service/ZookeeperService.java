package edu.baylor.zoo.service;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.jasypt.util.password.StrongPasswordEncryptor;

import edu.baylor.zoo.model.Zookeeper;
import edu.baylor.zoo.repositories.ZookeeperRepository;

@Stateless
public class ZookeeperService {

    private static String ZOOKEEPER_UNIQUE_LOGIN_MESSAGE = "Zookeeper must have unique login";
    
    private static StrongPasswordEncryptor spe = new StrongPasswordEncryptor();

    @Inject
    private Logger log;

    @Inject
    private Event<Zookeeper> zookeeperEventSrc;

    @Inject
    private ZookeeperRepository zookeeperRepo;

    public ServiceResponse save(Zookeeper keeper) {
        log.info("Registering " + keeper.getLogin());
        ServiceResponse response = new ServiceResponse();
        if (!isLoginAvailable(keeper.getLogin())) {
            response.setResult(false);
            response.setDescription(ZOOKEEPER_UNIQUE_LOGIN_MESSAGE);
            return response;
        }
        try {
            // Encrypt password before saving
            keeper.setPassword(spe.encryptPassword(keeper.getPassword()));
            zookeeperRepo.save(keeper);
        } catch (Exception ex) {
            response.setResult(false);
            response.setDescription("Error");
            log.severe(ex.toString());
            return response;
        }
        response.setResult(true);
        response.setDescription(String.format("%s, %s registered", keeper.getLogin(), keeper.getName()));
        zookeeperEventSrc.fire(keeper);
        log.info(String.format("result = %b, desc = %s", response.getResult(), response.getDescription()));
        return response;
    }

    public Zookeeper getZookeeper(String login, String password) {
    	Zookeeper zookeeper = zookeeperRepo.findOptionalByLogin(login);
        if (zookeeper != null && spe.checkPassword(password, zookeeper.getPassword())) {
            return zookeeper;
        }
        return null;
    }

    public boolean isLoginAvailable(String login) {
        return zookeeperRepo.findOptionalByLogin(login) == null;
    }
}
