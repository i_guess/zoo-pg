package edu.baylor.zoo.service;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import edu.baylor.zoo.model.Habitat;
import edu.baylor.zoo.repositories.HabitatRepository;

@Stateless
public class HabitatRegistration {
    
    private static String HABITAT_DUPLICATE_NAME_MESSAGE = "All habitats must have distinctive name.";
    
    @Inject
    private Logger log;
    
    @Inject
    private Event<Habitat> habitatEventSrc;
    
    @Inject
    private HabitatRepository habitatRepo;
    
    public ServiceResponse save(Habitat habitat) {
        log.info("Registering " + habitat.getName());
        ServiceResponse response = new ServiceResponse();

        if (habitatRepo.findOptionalByName(habitat.getName()) != null) {
            response.setResult(false);
            response.setDescription(HABITAT_DUPLICATE_NAME_MESSAGE);
            log.info(String.format("result = %b, desc = %s", response.getResult(), response.getDescription()));
            return response;
        }
        
        try {
            habitatRepo.save(habitat);
        } catch (Exception ex) {
            log.severe(ex.toString());
            response.setResult(false);
            response.setDescription("Error");
            return response;
        }
        habitatEventSrc.fire(habitat);
        response.setResult(true);
        response.setDescription(String.format("Habitat %s, %d was saved", habitat.getName(), habitat.getId()));
        log.info(String.format("result = %b, desc = %s", response.getResult(), response.getDescription()));
        return response;
    }
}
