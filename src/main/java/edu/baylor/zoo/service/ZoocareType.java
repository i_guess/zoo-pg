package edu.baylor.zoo.service;

public enum ZoocareType {
    FEEDING,
    CLEANING
}
