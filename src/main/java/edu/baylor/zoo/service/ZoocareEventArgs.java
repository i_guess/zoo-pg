package edu.baylor.zoo.service;

import java.util.Date;

public class ZoocareEventArgs {
    
    private ZoocareType type;
    private Long targetId;
    private Date dateTime;
    
    public ZoocareType getType() {
        return type;
    }
    public void setType(ZoocareType type) {
        this.type = type;
    }
    public Long getTargetId() {
        return targetId;
    }
    public void setTargetId(Long targetId) {
        this.targetId = targetId;
    }
    public Date getDateTime() {
        return dateTime;
    }
    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
