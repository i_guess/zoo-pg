package edu.baylor.zoo.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import edu.baylor.zoo.model.Animal;
import edu.baylor.zoo.model.Habitat;
import edu.baylor.zoo.model.Zookeeper;
import edu.baylor.zoo.repositories.AnimalRepository;
import edu.baylor.zoo.repositories.HabitatRepository;
import edu.baylor.zoo.repositories.ZookeeperRepository;

@Stateless
public class ZoocareService {
    
    private static String ZOOKEEPER_LEVEL_NOT_ENOUGH_MESSAGE = "Zookeeper does not have access rights to do this work";
    private static int LEVEL_TO_CARE_AGRESSIVE = 1;
    
    @Inject
    private Logger log;
    
    @Inject
    private HabitatRepository habitatRepo;
    
    @Inject
    private AnimalRepository animalRepo;
    
    @Inject
    private ZookeeperRepository zookeeperRepo;
    
    @Inject
    private Event<ZoocareEventArgs> zooCareEventSrc;
    
    public ServiceResponse clean(Long habitatId, Long zookeeperId) {
        log.info(zookeeperId + " cleans " + habitatId);
        ServiceResponse response = new ServiceResponse();
        Zookeeper keeper = zookeeperRepo.findOptionalById(zookeeperId);
        Habitat habitat = habitatRepo.findOptionalById(habitatId);
        if (keeper == null || habitat == null || !canClean(keeper, habitat)) {
            response.setResult(false);
            response.setDescription(ZOOKEEPER_LEVEL_NOT_ENOUGH_MESSAGE);
        }
        else {
            Date datetime = new Date();
            habitat.setLastCleaning(datetime);            
            try {
                habitatRepo.save(habitat);
            } catch (Exception ex) {
                log.severe(ex.toString());
                response.setResult(false);
                response.setDescription("Error");
                return response;
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
            String yourUtcDate = sdf.format(datetime);
            response.setResult(true);
            response.setDescription(yourUtcDate);
            ZoocareEventArgs args = new ZoocareEventArgs();
            args.setDateTime(datetime);
            args.setType(ZoocareType.CLEANING);
            args.setTargetId(habitatId);
            zooCareEventSrc.fire(args);
        }
        log.info(String.format("result = %b, desc = %s", response.getResult(), response.getDescription()));
        return response;
    }
    
    public ServiceResponse feed(Long animalId, Long zookeeperId) {
        log.info(zookeeperId + " feeds " + animalId);
        ServiceResponse response = new ServiceResponse();
        Zookeeper keeper = zookeeperRepo.findOptionalById(zookeeperId);
        Animal animal = animalRepo.findOptionalById(animalId);
        if (keeper == null || animal == null || !canFeed(keeper, animal)) {
            response.setResult(false);
            response.setDescription(ZOOKEEPER_LEVEL_NOT_ENOUGH_MESSAGE);
        }
        else {
            Date datetime = new Date();
            animal.setLastFeeding(datetime);         
            try {
                animalRepo.save(animal);
            } catch (Exception ex) {
                log.severe(ex.toString());
                response.setResult(false);
                response.setDescription("Error");
                return response;
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
            String yourUtcDate = sdf.format(datetime);
            response.setResult(true);
            response.setDescription(yourUtcDate);
            ZoocareEventArgs args = new ZoocareEventArgs();
            args.setDateTime(datetime);
            args.setType(ZoocareType.FEEDING);
            args.setTargetId(animalId);
            zooCareEventSrc.fire(args);
        }
        log.info(String.format("result = %b, desc = %s", response.getResult(), response.getDescription()));
        return response;
    }
    
    public boolean canClean(Zookeeper keeper, Habitat habitat) {
        if (keeper.getLevel() < LEVEL_TO_CARE_AGRESSIVE && habitat.getAnimals().size() > 0 && habitat.getAnimals().get(0).getDescription().getDangerous()) {
            return false;
        }
        return true;
    }
    
    public boolean canFeed(Zookeeper keeper, Animal animal) {
        if (keeper.getLevel() < LEVEL_TO_CARE_AGRESSIVE && animal.getDescription().getDangerous()) {
            return false;
        }
        return true;
    }
}
