package edu.baylor.zoo.controller;

import java.util.List;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import edu.baylor.zoo.data.AnimalListProducer;
import edu.baylor.zoo.model.Animal;

@Named
@RequestScoped
public class AnimalsPageController {
	
	@Inject
	private AnimalListProducer alp;
	
	public List<Animal> animals(){
		return alp.getAnimals();
	}
}
