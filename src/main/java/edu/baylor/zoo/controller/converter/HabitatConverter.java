package edu.baylor.zoo.controller.converter;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import edu.baylor.zoo.model.Habitat;
import edu.baylor.zoo.repositories.HabitatRepository;

@Named
@RequestScoped
public class HabitatConverter implements Converter, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private HabitatRepository deltaRepo;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if( value != null && value.trim().length() > 0) {
			Habitat habitat = deltaRepo.findOptionalById(Long.valueOf(value));
			return habitat;
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if(value != null) {
			return String.valueOf(((Habitat) value).getId());
		}
		else {
			return null;
		}
		
			
	}
}
