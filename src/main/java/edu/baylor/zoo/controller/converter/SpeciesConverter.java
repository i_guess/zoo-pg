package edu.baylor.zoo.controller.converter;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import edu.baylor.zoo.model.Species;
import edu.baylor.zoo.repositories.SpeciesRepository;

@Named
@RequestScoped
public class SpeciesConverter implements Converter, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private SpeciesRepository deltaRepo;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if( value != null && value.trim().length() > 0) {
			Species species = deltaRepo.findOptionalById(Long.valueOf(value));
			return species;
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if(value != null) {
			return String.valueOf(((Species) value).getId());
		}
		else {
			return null;
		}
		
			
	}

}
