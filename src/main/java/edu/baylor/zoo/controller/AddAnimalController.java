package edu.baylor.zoo.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import edu.baylor.zoo.data.HabitatListProducer;
import edu.baylor.zoo.data.SpeciesListProducer;
import edu.baylor.zoo.model.Animal;
import edu.baylor.zoo.model.Habitat;
import edu.baylor.zoo.model.Species;
import edu.baylor.zoo.service.AnimalRegistration;
import edu.baylor.zoo.service.ServiceResponse;

@Named
@ViewScoped
public class AddAnimalController implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Produces
    @Named
    private Animal newAnimal;

    @Inject
    private SpeciesListProducer slp;

    @Inject
    private HabitatListProducer hlp;

    @Inject
    private AnimalRegistration register;
    
    @Inject
    private FacesContext facesContext;

    @PostConstruct
    private void makeNewAnimal() {
        newAnimal = new Animal();
    }

    public Animal getNewAnimal() {
        return newAnimal;
    }

    public void setNewAnimal(Animal newAnimal) {
        this.newAnimal = newAnimal;

    }

    public String attemptSave() {
        ServiceResponse sr = register.save(newAnimal, newAnimal.getHabitat());
        if (sr.getResult()) {
            return "keeper.xhtml";
        }
        else {
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failure", sr.getDescription());
            facesContext.addMessage(null, m);
            facesContext.getExternalContext().getFlash().setKeepMessages(true);
            return "addAnimals.xhtml?faces-redirect=true";
        }
    }

    public List<Species> speciesToAdd(String query) {
        List<Species> species = new ArrayList<>();
        for (Species s : slp.getSpecies()) {
            if (s.getName().contains(query)) {
                species.add(s);
            }
        }
        return species;
    }

    public List<Habitat> habitatsAllowed(String query) {
        List<Habitat> habitats = new ArrayList<>();

        for (Habitat h : hlp.getHabitats()) {
            habitats.add(h);
        }

        return habitats;
    }

}
