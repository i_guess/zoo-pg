package edu.baylor.zoo.controller;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import edu.baylor.zoo.model.Zookeeper;
import edu.baylor.zoo.service.ZookeeperService;

@Named
@RequestScoped
public class LoginPageController {

    private static String ZOOKEEPER_LOGIN_SUCCESS = "Zookeeper successfully logged in";

    private static String ZOOKEEPER_LOGIN_FAILURE = "Zookeeper could not be logged in using login and password provided";

    @Inject
    private LoginController lc;

    @Inject
    private ZookeeperService zookeeperService;

    @Inject
    private FacesContext facesContext;

    private String login;

    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String checkLoginNecessary() {
        if (lc.getLoggedInUser() != null) {
            return "keeper.xhtml";
        }
        return null;
    }

    public String attemptLogin() {
        FacesMessage m;
        if (login == null || login.isEmpty() || password == null || password.isEmpty()) {
            m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failure", "Please enter a login and password");
            
            facesContext.addMessage(null, m);
            facesContext.getExternalContext().getFlash().setKeepMessages(true);
            return "login.xhtml?faces-redirect=true";
        }
        Zookeeper keeper = zookeeperService.getZookeeper(login, password);
        if (keeper != null) {
            // m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", ZOOKEEPER_LOGIN_SUCCESS);
            lc.setLoggedInUser(keeper);
            return "keeper.xhtml?faces-redirect=true";
        } else {
            m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failure", ZOOKEEPER_LOGIN_FAILURE);
            facesContext.addMessage(null, m);
            facesContext.getExternalContext().getFlash().setKeepMessages(true);
            return "login.xhtml?faces-redirect=true";
        }
    }

}