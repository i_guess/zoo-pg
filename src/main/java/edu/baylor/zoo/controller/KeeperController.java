package edu.baylor.zoo.controller;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import edu.baylor.zoo.controller.caretopic.ZoocareTopicSender;
import edu.baylor.zoo.model.Habitat;
import edu.baylor.zoo.model.Zookeeper;
import edu.baylor.zoo.service.ServiceResponse;
import edu.baylor.zoo.service.ZoocareService;
import edu.baylor.zoo.service.ZoocareType;

@Named
@RequestScoped
public class KeeperController implements Serializable {

    @Inject
    private LoginController lc;

    @Inject
    private ZoocareService careService;

    @Inject
    private ZoocareTopicSender senderBean;

    @Inject
    private FacesContext facesContext;

    @Inject
    private Logger log;

    public String clean(Long target_id) {
        FacesMessage m;
        Zookeeper keeper = lc.getLoggedInUser();
        if (keeper != null) {
            ServiceResponse resp = careService.clean(target_id, keeper.getId());
            log.info(resp.getDescription());
            if (resp.getResult()) {
                m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", resp.getDescription());
                senderBean.sendZoocare(ZoocareType.CLEANING, target_id, resp.getDescription());
                return null;
            } else {
                m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failure", resp.getDescription());
            }
            facesContext.addMessage(null, m);
        }
        return lc.checkLogin();
    }
    
    public String feed(Long target_id) {
        FacesMessage m;
        Zookeeper keeper = lc.getLoggedInUser();
        if (keeper != null) {
            ServiceResponse resp = careService.feed(target_id, keeper.getId());
            log.info(resp.getDescription());
            if (resp.getResult()) {
                m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", resp.getDescription());
                senderBean.sendZoocare(ZoocareType.FEEDING, target_id, resp.getDescription());
                return null;
            } else {
                m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failure", resp.getDescription());
            }
            facesContext.addMessage(null, m);
        }
        return null;
    }
    
    public boolean canwork(Habitat habitat) {
        return (lc.getLoggedInUser() != null && careService.canClean(lc.getLoggedInUser(), habitat));
    }
}
