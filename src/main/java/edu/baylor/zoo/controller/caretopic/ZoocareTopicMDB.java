package edu.baylor.zoo.controller.caretopic;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;



/**
 * <p>
 * A simple Message Driven Bean that asynchronously receives and processes the messages that are sent to the topic.
 * </p>
 *
 * @author Serge Pagop (spagop@redhat.com)
 */
@MessageDriven(name = "ZoocareTopicMDB", activationConfig = {
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "topic/zoocare"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")})
public class ZoocareTopicMDB implements MessageListener {
    
    @Inject
    Event<MapMessage> jmsEvent;

    @Inject
    private Logger log;
    
    /**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message rcvMessage) {
        MapMessage msg = null;
        try {
            if (rcvMessage instanceof MapMessage) {
                msg = (MapMessage) rcvMessage;
                log.info("ONJMSMessage Zoocare type: " + msg.getString("type") + " applied to:" + msg.getLong("target_id") + " at " + msg.getString("datetime"));
                jmsEvent.fire(msg);
            } else {
                log.info("ONJMSMessage wrong message type");
            }
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }
}