package edu.baylor.zoo.controller.caretopic;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Topic;

import edu.baylor.zoo.service.ZoocareType;

@JMSDestinationDefinitions(value = {
        @JMSDestinationDefinition(name = "java:/topic/zoocare", interfaceName = "javax.jms.Topic", destinationName = "ZoocareTopicMDB") })

@Stateless
public class ZoocareTopicSender {


    @Resource(lookup = "java:/topic/zoocare")
    private Topic topic;

    @Inject
    private JMSContext jmsContext;

    @Inject
    private Logger log;

    public void sendZoocare(ZoocareType careType, Long target_id, String datetime) {

        log.info("Sending Zoocare update " + careType + " " + target_id);
        try {
            MapMessage mapMsg = jmsContext.createMapMessage();
            mapMsg.setLong("target_id", target_id);
            mapMsg.setString("type", careType.toString());
            mapMsg.setString("datetime", datetime);
            jmsContext.createProducer().send(topic, mapMsg);
        } catch (JMSException e) {
            log.log(Level.SEVERE, "Error during send TeamState update occured", e);
        }
    }

}
