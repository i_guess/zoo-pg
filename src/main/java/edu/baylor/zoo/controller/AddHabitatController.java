package edu.baylor.zoo.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import edu.baylor.zoo.model.Habitat;
import edu.baylor.zoo.model.NaturalEnvironment;
import edu.baylor.zoo.service.HabitatRegistration;
import edu.baylor.zoo.service.ServiceResponse;

@Named
@ViewScoped
public class AddHabitatController implements Serializable {

    /**
    * 
    */
    private static final long serialVersionUID = 1L;

    @Produces
    @Named
    private Habitat newHabitat;

    @Inject
    private HabitatRegistration habitatRego;

    @PostConstruct
    private void setup() {
        newHabitat = new Habitat();
    }

    @Inject
    private FacesContext facesContext;

    
    public Habitat getNewHabitat() {
        return newHabitat;
    }

    public void setNewHabitat(Habitat newHabitat) {
        this.newHabitat = newHabitat;
    }

    public List<NaturalEnvironment> getEnvironments(String query) {
        List<NaturalEnvironment> environments = new ArrayList<>();
        for (NaturalEnvironment ne : NaturalEnvironment.class.getEnumConstants()) {
            environments.add(ne);
        }
        return environments;
    }

    public String attemptSave() throws Exception {
    	newHabitat.setLastCleaning(new Date());
        ServiceResponse sr = habitatRego.save(newHabitat);
        if (sr.getResult()) {
            return "keeper.xhtml";
        }
        else {
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failure", sr.getDescription());
            facesContext.addMessage(null, m);
            facesContext.getExternalContext().getFlash().setKeepMessages(true);
            return "addHabitat.xhtml?faces-redirect=true";
        }
    }
}
