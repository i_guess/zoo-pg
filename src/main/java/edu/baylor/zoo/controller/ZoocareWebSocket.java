package edu.baylor.zoo.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import edu.baylor.zoo.controller.caretopic.ZoocareTopicSender;

@ServerEndpoint("/keeper.xhtml")
public class ZoocareWebSocket implements Serializable {

    @Inject
    private ZoocareTopicSender senderBean;
    
    private static List<Session> allUsers = new ArrayList<>();

//    @OnMessage
//    public String teamStateChange(String typeAndTargetId) {
//        System.out.println("Say hello to '" + typeAndTargetId + "'");
//
//        String typeAndTargetIdSplit[] = typeAndTargetId.split(", ");
//        try {
//            ZoocareType type = ZoocareType.valueOf(typeAndTargetIdSplit[0]);
//            Long targetId = Long.parseLong(typeAndTargetIdSplit[1]);
//            senderBean.sendZoocare(type, targetId);
//            return ("We HaVe InFoRmEd YoUr PeErS!");
//        }
//        catch(Exception ex){
//            return ("Wrong format. Correct format: 'type, target_id'");
//        }
//    }

    @OnOpen
    public void helloOnOpen(Session session) {
        System.out.println("WebSocket opened: " + session.getId());
        allUsers.add(session);
        session.getAsyncRemote().sendText("connected");
    }

    @OnClose
    public void helloOnClose(Session sessionToClose, CloseReason reason) {
        allUsers.remove(sessionToClose);
        System.out.println("WebSocket connection closed with CloseCode: " + reason.getCloseCode());
    }
    
    public void onJMSEvent(final @Observes MapMessage msg) {
        Logger.getLogger(ZoocareWebSocket.class.getName()).log(Level.INFO, "Got JMS Message!");
        final String message;
        try {
            message = String.format("%s, %d, %s", msg.getString("type"), msg.getLong("target_id"), msg.getString("datetime"));
            allUsers.parallelStream().forEach(s -> s.getAsyncRemote().sendText(message));
        } catch (JMSException ex) {
            Logger.getLogger(ZoocareWebSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
