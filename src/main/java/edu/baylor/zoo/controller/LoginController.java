package edu.baylor.zoo.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import edu.baylor.zoo.model.Zookeeper;

@Named
@SessionScoped
public class LoginController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Zookeeper loggedInUser = null;

	public Zookeeper getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(Zookeeper loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	
	public String checkLogin() {
		if(loggedInUser == null) {
			return "login.xhtml";
		}
		return null;
	}
	
	public String checkManager() {
		if(loggedInUser != null && loggedInUser.isManager()) {
			return null;
		}
		return "login.xhtml";
	}
	
	public String logout() {
		loggedInUser = null;
		return "login.xhtml";
	}
	
}
