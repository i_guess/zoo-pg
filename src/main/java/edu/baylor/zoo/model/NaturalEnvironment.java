package edu.baylor.zoo.model;

public enum NaturalEnvironment {
    FOREST,
    JUNGLE,
    POOL,
    AQUARIUM,
    DESERT,
    TERRARIUM,
    TUNDRA,
    PLAINS
}
