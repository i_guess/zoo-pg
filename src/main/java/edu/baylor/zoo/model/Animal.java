package edu.baylor.zoo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

@Entity
public class Animal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    @Past
    @Temporal(TemporalType.DATE)
    private Date birthDate;

    @NotNull
    @ManyToOne
    private Species description;

    @NotNull
    @ManyToOne(cascade = CascadeType.ALL)
    private Habitat habitat;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastFeeding;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Species getDescription() {
        return description;
    }

    public void setDescription(Species description) {
        this.description = description;
    }

    public Habitat getHabitat() {
        return habitat;
    }

    public void setHabitat(Habitat habitat) {
        this.habitat = habitat;
    }

    public Date getLastFeeding() {
        return lastFeeding;
    }

    public void setLastFeeding(Date lastFeeding) {
        this.lastFeeding = lastFeeding;
    }

}
