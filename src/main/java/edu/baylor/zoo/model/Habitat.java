package edu.baylor.zoo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "name"))
public class Habitat implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    private NaturalEnvironment type;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastCleaning;

    @OneToMany(mappedBy = "habitat", cascade = CascadeType.ALL)
    @OrderBy("id")
    private List<Animal> animals = new ArrayList<Animal>();
    
    @NotNull
    @Min(1)
    private int capacity = 1;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NaturalEnvironment getType() {
        return type;
    }

    public void setType(NaturalEnvironment type) {
        this.type = type;
    }

    public Date getLastCleaning() {
        return lastCleaning;
    }

    public void setLastCleaning(Date lastCleaning) {
        this.lastCleaning = lastCleaning;
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    public void setAnimals(List<Animal> animals) {
        this.animals = animals;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
