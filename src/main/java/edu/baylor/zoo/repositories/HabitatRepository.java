package edu.baylor.zoo.repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;

import edu.baylor.zoo.model.Habitat;
import edu.baylor.zoo.model.NaturalEnvironment;
import edu.baylor.zoo.model.Species;

@Repository(forEntity = Habitat.class)
public interface HabitatRepository extends EntityRepository<Habitat, Long> {
    
    Habitat findOptionalById(Long id);
    Habitat findOptionalByName(String name);
    List<Habitat> findAll();
    List<Habitat> findAllOrderByIdAsc();
    
    @Query("select distinct h from Habitat h inner join h.animals a where h.animals.size > 0 and h.animals.size < h.capacity and a.description = ?1")
    List<Habitat> findVacantFriends(Species species);
    
    @Query("select h from Habitat h where h.animals.size = 0 and h.type = ?1")
    List<Habitat> findEmptyByEnvironment(NaturalEnvironment env);
}
