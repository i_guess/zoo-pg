package edu.baylor.zoo.repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import edu.baylor.zoo.model.Species;

@Repository(forEntity = Species.class)
public interface SpeciesRepository extends EntityRepository<Species, Long> {
    
    Species findOptionalById(Long id);
    List<Species> findAll();
    
}
