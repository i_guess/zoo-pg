package edu.baylor.zoo.repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import edu.baylor.zoo.model.Zookeeper;

@Repository(forEntity = Zookeeper.class)
public interface ZookeeperRepository extends EntityRepository<Zookeeper, Long> {
    
    Zookeeper findOptionalById(Long id);
    Zookeeper findOptionalByLogin(String login);
    Zookeeper findOptionalByLoginAndPassword(String login, String password);
    List<Zookeeper> findAll();
    
}
