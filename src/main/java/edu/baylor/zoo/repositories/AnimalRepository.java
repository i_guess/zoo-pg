package edu.baylor.zoo.repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import edu.baylor.zoo.model.Animal;

@Repository(forEntity = Animal.class)
public interface AnimalRepository extends EntityRepository<Animal, Long> {

    Animal findOptionalById(Long id);
    List<Animal> findAll();    
    List<Animal> findAllOrderByIdAsc();

}
