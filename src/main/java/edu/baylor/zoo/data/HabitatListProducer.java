package edu.baylor.zoo.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import edu.baylor.zoo.model.Habitat;
import edu.baylor.zoo.repositories.HabitatRepository;

@Named
@RequestScoped
public class HabitatListProducer {

    @Inject
    private HabitatRepository deltaRepo;

    private List<Habitat> habitats;
    
    @Produces
    @Named
    public List<Habitat> getHabitats() {
        return habitats;
    }
    
    @PostConstruct
    public void retrieveAllHabitats() {
        habitats = deltaRepo.findAllOrderByIdAsc();
    }
    
    public void onHabitatListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final Habitat habitat) {
        retrieveAllHabitats();
    }
}