package edu.baylor.zoo.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import edu.baylor.zoo.model.Zookeeper;
import edu.baylor.zoo.repositories.ZookeeperRepository;


@RequestScoped
public class ZookeeperListProducer {

    @Inject
    private ZookeeperRepository deltaRepo;

    private List<Zookeeper> zookeepers;
    
    @Produces
    @Named
    public List<Zookeeper> getZookeepers() {
        return zookeepers;
    }
    
    @PostConstruct
    public void retrieveAllZookeepers() {
        zookeepers = deltaRepo.findAll();
    }
    
    public void onZookeeperListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final Zookeeper zookeeper) {
        retrieveAllZookeepers();
    }
}