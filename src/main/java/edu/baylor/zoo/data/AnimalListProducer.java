package edu.baylor.zoo.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import edu.baylor.zoo.model.Animal;
import edu.baylor.zoo.repositories.AnimalRepository;

@RequestScoped
public class AnimalListProducer {

    @Inject
    private AnimalRepository deltaRepo;

    private List<Animal> animals;
    
    @Produces
    @Named
    public List<Animal> getAnimals() {
        return animals;
    }
    
    @PostConstruct
    public void retrieveAllAnimals() {
        animals = deltaRepo.findAllOrderByIdAsc();
    }
    
    public void onAnimalListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final Animal animal) {
        retrieveAllAnimals();
    }
}
