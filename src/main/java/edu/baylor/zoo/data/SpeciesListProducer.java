package edu.baylor.zoo.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import edu.baylor.zoo.model.Species;
import edu.baylor.zoo.repositories.SpeciesRepository;

@Named
@RequestScoped
public class SpeciesListProducer {

    @Inject
    private SpeciesRepository deltaRepo;

    private List<Species> species;
    
    @Produces
    @Named
    public List<Species> getSpecies() {
        return species;
    }
    
    @PostConstruct
    public void retrieveAllSpecies() {
        species = deltaRepo.findAll();
    }
    
    public void onSpeciesListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final Species species) {
        retrieveAllSpecies();
    }
}