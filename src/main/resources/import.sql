--
-- JBoss, Home of Professional Open Source
-- Copyright 2015, Red Hat, Inc. and/or its affiliates, and individual
-- contributors by the @authors tag. See the copyright.txt in the
-- distribution for a full listing of individual contributors.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- http://www.apache.org/licenses/LICENSE-2.0
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

-- You can use this file to load seed data into the database using SQL statements
insert into zookeeper (name, login, password, level, ismanager) values ('master', 'testlogin', 'JpMwxxeIavyU+VSHK38QY75v2X17ulQtPhlbqrbM0TKXKqspSjmBu65Hpr5jv4rG', 1, true)
insert into zookeeper (name, login, password, level, ismanager) values ('newmanager', 'test2', 'JpMwxxeIavyU+VSHK38QY75v2X17ulQtPhlbqrbM0TKXKqspSjmBu65Hpr5jv4rG', 0, false)
insert into species (name, description, dangerous, naturalEnvironment) values ('squirrel', 'none', false, 'JUNGLE');
insert into species (name, description, dangerous, naturalEnvironment) values ('wolf', 'none', true, 'FOREST');
insert into species (name, description, dangerous, naturalEnvironment) values ('bear', 'angry bear', true, 'FOREST');
insert into species (name, description, dangerous, naturalEnvironment) values ('tiger', 'rrrrrrr', true, 'PLAINS');
insert into species (name, description, dangerous, naturalEnvironment) values ('white bear', 'white', true, 'TUNDRA');
insert into species (name, description, dangerous, naturalEnvironment) values ('seal', 'white', false, 'POOL');
insert into habitat (name, type, lastcleaning, capacity) values ('Squirrels Jungle', 'JUNGLE', '2017-11-27', 5);
insert into habitat (name, type, lastcleaning, capacity) values ('Forest for wolfs', 'FOREST', '2017-11-27', 5);
insert into habitat (name, type, lastcleaning, capacity) values ('Seals Pool', 'POOL', '2017-11-27', 3);
insert into habitat (name, type, lastcleaning, capacity) values ('Tundra', 'TUNDRA', '2017-11-27', 1);
insert into animal (name, birthDate, description_id, lastFeeding, habitat_id) values ('squirrel1', '2017-11-27', 1, '2017-11-27', 1);
insert into animal (name, birthDate, description_id, lastFeeding, habitat_id) values ('squirrel2', '2017-11-27', 1, '2017-11-27', 1);
insert into animal (name, birthDate, description_id, lastFeeding, habitat_id) values ('squirrel3', '2017-11-27', 1, '2017-11-27', 1);
insert into animal (name, birthDate, description_id, lastFeeding, habitat_id) values ('wolf1', '2017-11-27', 2, '2017-11-27', 2);
insert into animal (name, birthDate, description_id, lastFeeding, habitat_id) values ('wolf2', '2017-11-27', 2, '2017-11-27', 2);
insert into animal (name, birthDate, description_id, lastFeeding, habitat_id) values ('seal1', '2017-11-27', 6, '2017-11-27', 3);
insert into animal (name, birthDate, description_id, lastFeeding, habitat_id) values ('seal2', '2017-11-27', 6, '2017-11-27', 3);
insert into animal (name, birthDate, description_id, lastFeeding, habitat_id) values ('seal3', '2017-11-27', 6, '2017-11-27', 3);


