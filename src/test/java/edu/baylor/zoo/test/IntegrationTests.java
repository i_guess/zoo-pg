package edu.baylor.zoo.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.sql.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import edu.baylor.zoo.model.Animal;
import edu.baylor.zoo.model.Habitat;
import edu.baylor.zoo.model.NaturalEnvironment;
import edu.baylor.zoo.model.Species;
import edu.baylor.zoo.model.Zookeeper;
import edu.baylor.zoo.repositories.AnimalRepository;
import edu.baylor.zoo.repositories.HabitatRepository;
import edu.baylor.zoo.repositories.SpeciesRepository;
import edu.baylor.zoo.repositories.ZookeeperRepository;
import edu.baylor.zoo.service.AnimalRegistration;
import edu.baylor.zoo.service.HabitatRegistration;
import edu.baylor.zoo.service.ServiceResponse;
import edu.baylor.zoo.service.ZoocareService;
import edu.baylor.zoo.service.ZookeeperService;

@RunWith(Arquillian.class)
public class IntegrationTests {

    @Deployment
    public static Archive<?> createTestArchive() {

        File[] files = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeDependencies().resolve()
                .withTransitivity().asFile();
        String deltaSpikeProps = "globalAlternatives.org.apache.deltaspike.jpa.spi.transaction.TransactionStrategy="
                + "org.apache.deltaspike.jpa.impl.transaction.BeanManagedUserTransactionStrategy";
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackages(true, "edu.baylor.zoo.data", "edu.baylor.zoo.model", "edu.baylor.zoo.repositories", "edu.baylor.zoo.service", "edu.baylor.zoo.util")
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new StringAsset(deltaSpikeProps), "classes/META-INF/apache-deltaspike.properties")
                .addAsLibraries(files)
                // Deploy our test datasource
                .addAsWebInfResource("test-ds.xml");
    }

    @Inject
    private Logger log;

    @Inject
    private AnimalRegistration animalRegService;

    @Inject 
    private HabitatRegistration habitatRegService;
    
    @Inject
    private ZookeeperService keeperService;
    
    @Inject
    private ZoocareService careService;
    
    @Inject
    private SpeciesRepository speciesRepo;

    @Inject
    private HabitatRepository habitatRepo;

    @Inject
    private AnimalRepository animalRepo;

    @Inject
    private ZookeeperRepository zookeeperRepo;
    
    private Animal testAnimal;

    private Habitat testHabitat;

    private Species testSpecies;

    private Zookeeper testZookeeper;
    
    @Before
    public void setUp() throws Exception {
        testSpecies = new Species();
        testSpecies.setDescription("Zerglings");
        testSpecies.setName("Pecan squirrels");
        testSpecies.setDangerous(false);
        testSpecies.setNaturalEnvironment(NaturalEnvironment.FOREST);

        testHabitat = new Habitat();
        testHabitat.setCapacity(10);
        testHabitat.setName("Pecan trees");
        testHabitat.setType(NaturalEnvironment.FOREST);

        testAnimal = new Animal();
        testAnimal.setDescription(testSpecies);
        testAnimal.setName("Leeloo");
        testAnimal.setBirthDate(Date.valueOf("2017-10-17"));
        
        testZookeeper = new Zookeeper();
        testZookeeper.setLevel(0);
        testZookeeper.setLogin("testlogin");
        testZookeeper.setPassword("testpass");
        testZookeeper.setName("tester");
    }

    @After
    public void tearDown() {
        for (Animal an : animalRepo.findAll()) {
            animalRepo.attachAndRemove(an);
        }

        for (Species sp : speciesRepo.findAll()) {
            speciesRepo.attachAndRemove(sp);
        }

        for (Habitat ha : habitatRepo.findAll()) {
            habitatRepo.attachAndRemove(ha);
        }

        for (Zookeeper keeper : zookeeperRepo.findAll()) {
            zookeeperRepo.attachAndRemove(keeper);
        }
        
        testAnimal = null;
        testSpecies = null;
        testHabitat = null;
        testZookeeper = null;
    }

    @Test
    public void RegisterAnimalPositive() throws Exception {
        speciesRepo.save(testSpecies);
        habitatRepo.save(testHabitat);

        ServiceResponse resp = animalRegService.save(testAnimal, testHabitat);
        assertNotNull(testAnimal.getId());
        assertTrue(resp.getResult());
        assertEquals(String.format("%s, %d thrown to the cage %s, %d", testAnimal.getName(), testAnimal.getId(),
                testHabitat.getName(), testHabitat.getId()), resp.getDescription());
    }

    @Test
    public void RegisterAnimalNegativeBadEnvironment() throws Exception {
        testSpecies.setNaturalEnvironment(NaturalEnvironment.JUNGLE);
        speciesRepo.save(testSpecies);
        habitatRepo.save(testHabitat);

        ServiceResponse resp = animalRegService.save(testAnimal, testHabitat);
        assertNull(testAnimal.getId());
        assertFalse(resp.getResult());
        assertEquals(
                "Animal can be placed only to a habitat of proper type that is not full. Animal can not be placed to animals of other species.",
                resp.getDescription());
    }

    @Test
    public void RegisterAnimalNegativeNoSlotsInHabitat() throws Exception {
        testHabitat.setCapacity(1);
        speciesRepo.save(testSpecies);
        habitatRepo.save(testHabitat);

        animalRegService.save(testAnimal, testHabitat);

        Animal testAnimal2 = new Animal();
        testAnimal2.setDescription(testSpecies);
        testAnimal2.setName("Leeloo2");
        testAnimal2.setBirthDate(Date.valueOf("2017-10-26"));

        ServiceResponse resp = animalRegService.save(testAnimal2, testHabitat);

        assertNull(testAnimal2.getId());
        assertFalse(resp.getResult());
        assertEquals(
                "Animal can be placed only to a habitat of proper type that is not full. Animal can not be placed to animals of other species.",
                resp.getDescription());
    }

    @Test
    public void RegisterAnimalNegativeOtherSpeciesInside() throws Exception {
        speciesRepo.save(testSpecies);
        habitatRepo.save(testHabitat);

        animalRegService.save(testAnimal, testHabitat);

        Species species2 = new Species();
        species2.setDangerous(true);
        species2.setDescription("Very angry");
        species2.setName("Bear");
        species2.setNaturalEnvironment(testSpecies.getNaturalEnvironment());
        speciesRepo.save(species2);

        Animal testAnimal2 = new Animal();
        testAnimal2.setDescription(species2);
        testAnimal2.setName("Baptist James");
        testAnimal2.setBirthDate(Date.valueOf("2017-09-26"));

        ServiceResponse resp = animalRegService.save(testAnimal2, testHabitat);

        assertNull(testAnimal2.getId());
        assertFalse(resp.getResult());
        assertEquals(
                "Animal can be placed only to a habitat of proper type that is not full. Animal can not be placed to animals of other species.",
                resp.getDescription());

    }

    @Test
    public void RegisterAnimalFindFriends() throws Exception {
        speciesRepo.save(testSpecies);
        habitatRepo.save(testHabitat);
        animalRegService.save(testAnimal, testHabitat);

        Animal testAnimal2 = new Animal();
        testAnimal2.setDescription(testSpecies);
        testAnimal2.setName("Squirrel 2");
        testAnimal2.setBirthDate(Date.valueOf("2000-01-01"));

        List<Habitat> result = animalRegService.findFriends(testAnimal2.getDescription());
        assertEquals(1, result.size());
        assertEquals(testHabitat.getId(), result.get(0).getId());
        assertEquals(result, animalRegService.getHabitatSuggestionList(testSpecies));
    }

    @Test
    public void RegisterAnimalFindEmptyHabitat() throws Exception {
        speciesRepo.save(testSpecies);
        habitatRepo.save(testHabitat);
        animalRegService.save(testAnimal, testHabitat);
        
        Habitat testHabitat2 = new Habitat();
        testHabitat2.setCapacity(10);
        testHabitat2.setName("Another forest");
        testHabitat2.setType(NaturalEnvironment.FOREST);
        habitatRepo.save(testHabitat2);
        
        Species species2 = new Species();
        species2.setDangerous(true);
        species2.setDescription("Very angry");
        species2.setName("Bear");
        species2.setNaturalEnvironment(testSpecies.getNaturalEnvironment());
        speciesRepo.save(species2);

        Animal testAnimal2 = new Animal();
        testAnimal2.setDescription(species2);
        testAnimal2.setName("Baptist James");
        testAnimal2.setBirthDate(Date.valueOf("2017-09-26"));
        
        List<Habitat> result = animalRegService.findFriends(testAnimal2.getDescription());
        assertEquals(0, result.size());
        
        result = animalRegService.findEmptyHabitats(testAnimal2.getDescription().getNaturalEnvironment());
        assertEquals(1, result.size());
        assertEquals(testHabitat2.getId(), result.get(0).getId());
    }
    
    @Test
    public void RegisterAnimalFindSuggestionListOrderFriendsFirst() throws Exception {
        speciesRepo.save(testSpecies);
        
        Habitat testHabitat2 = new Habitat();
        testHabitat2.setCapacity(10);
        testHabitat2.setName("Another forest");
        testHabitat2.setType(NaturalEnvironment.FOREST);
        habitatRepo.save(testHabitat2);
        
        // save friend and it's habitat after testHabitat2 to see if it will be placed first in suggestion list
        animalRegService.save(testAnimal, testHabitat);
        habitatRepo.save(testHabitat);

        Animal testAnimal2 = new Animal();
        testAnimal2.setDescription(testAnimal.getDescription());
        testAnimal2.setName("Squirrel2");
        testAnimal2.setBirthDate(Date.valueOf("2017-09-26"));

        
        List<Habitat> result = animalRegService.getHabitatSuggestionList(testAnimal2.getDescription());
        assertEquals(2, result.size());
        assertEquals(testHabitat.getId(), result.get(0).getId());
        assertEquals(testHabitat2.getId(), result.get(1).getId());
    }
    
    @Test
    public void RegisterHabitatPositive() {
        ServiceResponse response = habitatRegService.save(testHabitat);
        assertTrue(response.getResult());
        assertEquals(String.format("Habitat %s, %d was saved", testHabitat.getName(), testHabitat.getId()), response.getDescription());
        assertNotNull(testHabitat.getId());
    }
    
    @Test
    public void RegisterHabitatNegativeTheSameName() {
        habitatRegService.save(testHabitat);
        
        Habitat habitat2 = new Habitat();
        habitat2.setCapacity(3);
        habitat2.setType(NaturalEnvironment.POOL);
        habitat2.setName(testHabitat.getName());
        
        ServiceResponse response = habitatRegService.save(habitat2);
        assertFalse(response.getResult());
        assertEquals("All habitats must have distinctive name.", response.getDescription());
        assertNull(habitat2.getId());
    }
    
    @Test
    public void RegisterZookeeperPositive() {
        ServiceResponse response = keeperService.save(testZookeeper);
        assertNotNull(testZookeeper.getId());
        assertTrue(response.getResult());
        assertEquals(testZookeeper.getLogin() + ", " + testZookeeper.getName() + " registered", response.getDescription());
        assertEquals(testZookeeper.getId(), keeperService.getZookeeper("testlogin", "testpass").getId());
    }
    
    @Test
    public void RegisterZookeeperNegativeTheSameLogin() {
        keeperService.save(testZookeeper);
        
        Zookeeper keeper2 = new Zookeeper();
        keeper2.setLogin(testZookeeper.getLogin());
        keeper2.setName("other name");
        keeper2.setPassword("random pass");
        assertFalse(keeperService.isLoginAvailable(keeper2.getLogin()));

        ServiceResponse response = keeperService.save(testZookeeper);
        assertNull(keeper2.getId());
        assertFalse(response.getResult());
        assertEquals("Zookeeper must have unique login", response.getDescription());
        assertNull(keeperService.getZookeeper(keeper2.getLogin(), keeper2.getPassword()));
    }
    
    @Test
    public void CanCleanPositive0Level() {
        testSpecies.setDangerous(false);
        speciesRepo.save(testSpecies);
        habitatRepo.save(testHabitat);
        animalRegService.save(testAnimal, testHabitat);
        testZookeeper.setLevel(0);
        keeperService.save(testZookeeper);
        
        assertTrue(careService.canClean(testZookeeper, testHabitat));
        ServiceResponse response = careService.clean(testHabitat.getId(), testZookeeper.getId());
        assertTrue(response.getResult());
        assertNotNull(testHabitat.getLastCleaning());
        

    }
    
    @Test
    public void CanFeedPositive0Level() {
        testSpecies.setDangerous(false);
        speciesRepo.save(testSpecies);
        habitatRepo.save(testHabitat);
        animalRegService.save(testAnimal, testHabitat);
        testZookeeper.setLevel(0);
        keeperService.save(testZookeeper);
        
        assertTrue(careService.canFeed(testZookeeper, testAnimal));
        ServiceResponse response = careService.feed(testAnimal.getId(), testZookeeper.getId());
        assertTrue(response.getResult());
        assertNotNull(testAnimal.getLastFeeding());
    }
    
    @Test
    public void CanCleanPositive1Level() {
        testSpecies.setDangerous(true);
        speciesRepo.save(testSpecies);
        habitatRepo.save(testHabitat);
        animalRegService.save(testAnimal, testHabitat);
        testZookeeper.setLevel(1);
        keeperService.save(testZookeeper);
        
        assertTrue(careService.canClean(testZookeeper, testHabitat));
        ServiceResponse response = careService.clean(testHabitat.getId(), testZookeeper.getId());
        assertTrue(response.getResult());
        assertNotNull(testHabitat.getLastCleaning());
    }
    
    @Test
    public void CanFeedPositive1Level() {
        testSpecies.setDangerous(true);
        speciesRepo.save(testSpecies);
        habitatRepo.save(testHabitat);
        animalRegService.save(testAnimal, testHabitat);
        testZookeeper.setLevel(1);
        keeperService.save(testZookeeper);
        
        assertTrue(careService.canFeed(testZookeeper, testAnimal));
        ServiceResponse response = careService.feed(testAnimal.getId(), testZookeeper.getId());
        assertTrue(response.getResult());
        assertNotNull(testAnimal.getLastFeeding());
    }
    
    @Test
    public void CanCleanNegative1Level() {
        testSpecies.setDangerous(true);
        speciesRepo.save(testSpecies);
        habitatRepo.save(testHabitat);
        animalRegService.save(testAnimal, testHabitat);
        testZookeeper.setLevel(0);
        keeperService.save(testZookeeper);
        
        assertFalse(careService.canClean(testZookeeper, testHabitat));
        ServiceResponse response = careService.clean(testHabitat.getId(), testZookeeper.getId());
        assertFalse(response.getResult());
        assertEquals("Zookeeper does not have access rights to do this work", response.getDescription());
        assertNull(testHabitat.getLastCleaning());
    }
    
    @Test
    public void CanFeedNegative1Level() {
        testSpecies.setDangerous(true);
        speciesRepo.save(testSpecies);
        habitatRepo.save(testHabitat);
        animalRegService.save(testAnimal, testHabitat);
        testZookeeper.setLevel(0);
        keeperService.save(testZookeeper);
        
        assertFalse(careService.canFeed(testZookeeper, testAnimal));
        ServiceResponse response = careService.feed(testAnimal.getId(), testZookeeper.getId());
        assertFalse(response.getResult());
        assertEquals("Zookeeper does not have access rights to do this work", response.getDescription());        
        assertNull(testAnimal.getLastFeeding());
    }
}
